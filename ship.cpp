#include "ship.h"

namespace SeaWar
{

	CShip :: CShip() :  m_uiBeginCellPosition( 0 ), 
						m_uiEndCellPosition( 0 ), 
						m_uiLenght( 0 ),
						m_bIsPlacedHorizontal( true )
				 
	{

	}

	CShip :: CShip( unsigned int uiBeginCellPosition, 
				    unsigned int uiEndCellPosition,
					unsigned int uiLenght,
				    bool bIsPlacedHorizontal
				  )
				  :
				  m_uiBeginCellPosition( uiBeginCellPosition ),
				  m_uiEndCellPosition( uiEndCellPosition ),
				  m_uiLenght( uiLenght ),
				  m_bIsPlacedHorizontal( bIsPlacedHorizontal )
	
	{
		
	}

	CShip :: ~CShip()
	{
	}

	
	unsigned int CShip :: BeginCellPosition() const
	{
		return m_uiBeginCellPosition;
	}

	unsigned int CShip :: EndCellPosition() const
	{
		return m_uiEndCellPosition;
	}

	unsigned int CShip :: Lenght() const
	{
		return m_uiLenght;
	}

	bool CShip :: IsPlacedHorizontal() const
	{
		return m_bIsPlacedHorizontal;
	}
	
}