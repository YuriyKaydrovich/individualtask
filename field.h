#ifndef FIELD_H
#define FIELD_H

#include "sea_war_precompiled.h"

namespace SeaWar
{
	class CFieldCell; 

	typedef std::vector<CFieldCell> CellVector;
	typedef CellVector::iterator CellVectorIterator;
	
	class CFieldCell
	{
		private:
			enum EFieldCellValue
			{
				CELL_EMPTY,
				CELL_MISS,
				CELL_HIT,
				CELL_SHIP_PART
			};
		private:
			EFieldCellValue m_fcvCellValue;
			
		public:
			CFieldCell():m_fcvCellValue( CELL_EMPTY ) {};
			~CFieldCell(){};

			bool IsEmpty() const { return m_fcvCellValue == CELL_EMPTY; }
			bool IsShipPart() const { return m_fcvCellValue == CELL_SHIP_PART; }
			bool IsMiss() const { return m_fcvCellValue == CELL_MISS; }
			bool IsHit() const { return m_fcvCellValue == CELL_HIT; }

			void SetEmpty() { m_fcvCellValue = CELL_EMPTY; }
			void SetShipPart() { m_fcvCellValue = CELL_SHIP_PART;}
			void SetMiss() { m_fcvCellValue = CELL_MISS; }
			void SetHit() { m_fcvCellValue = CELL_HIT; }
	};

	
	class CField
	{
		private:
			unsigned int m_uiCellsCount;
			CFieldCell* m_pfcFieldCells;
				
		public:
			CField( unsigned int uiCellsCount) : m_uiCellsCount( uiCellsCount )
			{
				m_pfcFieldCells = new CFieldCell[ uiCellsCount ];
			}

			~CField()
			{
				if( m_pfcFieldCells )
				{
					delete [] m_pfcFieldCells;
					m_pfcFieldCells = NULL;
				}
			}

			CFieldCell& CellAtPosition( int iPosition )
			{
				return m_pfcFieldCells[ iPosition ];
			}

			CFieldCell& CellAtPosition( int iPosition ) const
			{
				return m_pfcFieldCells[ iPosition ];
			}
			

			void ClearCells()
			{
				memset( m_pfcFieldCells, 0, m_uiCellsCount * sizeof( CFieldCell ) );
			}

			unsigned int CellsCount()
			{
				return m_uiCellsCount;
			}
	};

}

#endif /* FIELD_H*/