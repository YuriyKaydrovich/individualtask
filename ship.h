#ifndef SHIP_H
#define SHIP_H

#include "sea_war_precompiled.h"

namespace SeaWar
{
	
	class CShip
	{
		private:
			unsigned int m_uiBeginCellPosition;
			unsigned int m_uiEndCellPosition;
			unsigned int m_uiLenght;
			bool m_bIsPlacedHorizontal;
			
		public:
			CShip();
			CShip( unsigned int uiBeginCellPosition, 
				   unsigned int uiEndCellPosition,
				   unsigned int uiLenght,
				   bool bIsPlacedHorizontal
				 );
			~CShip();

			void SetBeginCellPosition( unsigned int uiPosition );
			void SetEndCellPosition( unsigned int uiPosition );
			void SetLenght( unsigned int uiCount );
			void SetPlacedHorizontal( bool bIsPlacedHorizontal );

			unsigned int BeginCellPosition() const;
			unsigned int EndCellPosition() const;
			unsigned int Lenght() const;
			bool IsPlacedHorizontal() const;

	};
}

#endif /* SHIP_H */