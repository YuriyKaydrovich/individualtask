#include "model.h"
using namespace std;

namespace SeaWar
{
	
	CSeaWarModel :: CSeaWarModel() : m_uiOwnShipsCount( 0 )							 
	{
		m_pfOwnField = new CField( k_uiFieldSize * k_uiFieldSize );
		m_pfEnemyField = new CField( k_uiFieldSize * k_uiFieldSize );
		InitDefaultShipsLenghts();
	}

	CSeaWarModel :: ~CSeaWarModel()
	{
		delete m_pfEnemyField;
		delete m_pfOwnField;

		m_pfEnemyField = NULL;
		m_pfOwnField = NULL;
	}

	int CSeaWarModel :: GetShipRectTopLeft( int iBeginPosition ) const
	{
		int iTopLeftRow;
		int iTopLeftCol;

		( ( iTopLeftRow = iBeginPosition / k_uiFieldSize - 1 ) < 0 ) ?  iTopLeftRow += 1 :  iTopLeftRow;
		( ( iTopLeftCol = iBeginPosition % k_uiFieldSize - 1 ) < 0 ) ?  iTopLeftCol += 1 :  iTopLeftCol;

		return iTopLeftRow * k_uiFieldSize + iTopLeftCol;
		
	}

	int CSeaWarModel :: GetShipRectBottomRight( int iEndPosition ) const
	{
		
		int iBottomRightRow = 0;
		int iBottomRightCol = 0;

		( ( iBottomRightRow = iEndPosition / k_uiFieldSize + 1 ) == k_uiFieldSize ) ?  iBottomRightRow -= 1 :  iBottomRightRow;
		( ( iBottomRightCol = iEndPosition % k_uiFieldSize + 1 ) == k_uiFieldSize ) ?  iBottomRightCol -= 1 :  iBottomRightCol;

		return iBottomRightRow * k_uiFieldSize + iBottomRightCol;
	}

	

	bool CSeaWarModel :: IsRectEmpty( int iRectTopLeft, int iRectBottomRight ) const
	{
		bool bIsEmpty = true;

		int iRow = 0, iCol = 0;
		int iMinRow = iRectTopLeft / k_uiFieldSize, iMaxRow = iRectBottomRight / k_uiFieldSize;
		int iMinCol = iRectTopLeft % k_uiFieldSize, iMaxCol = iRectBottomRight % k_uiFieldSize;

		for( iRow = iMinRow; iRow <= iMaxRow; ++iRow )
		{
			for( iCol = iMinCol; iCol <= iMaxCol; ++iCol )
			{
				bIsEmpty = m_pfOwnField ->CellAtPosition( iRow * k_uiFieldSize + iCol ).IsEmpty();

				if( !bIsEmpty )
				{
					break;
				}
			}

			if( !bIsEmpty )
			{
				break;
			}
		}

		return bIsEmpty;
	}

	void CSeaWarModel :: MarkDestroyedEnemyShip( int iRectTopLeft, int iRectBottomRight )
	{
		int iRow = 0, iCol = 0;
		int iMinRow = iRectTopLeft / k_uiFieldSize, iMaxRow = iRectBottomRight / k_uiFieldSize;
		int iMinCol = iRectTopLeft % k_uiFieldSize, iMaxCol = iRectBottomRight % k_uiFieldSize;

		for( iRow = iMinRow; iRow <= iMaxRow; ++iRow )
		{

			for( iCol = iMinCol; iCol <= iMaxCol; ++iCol )
			{
				
				CFieldCell &fcCurrentCell = m_pfOwnField -> CellAtPosition( iRow * k_uiFieldSize + iCol );
				
				if( !fcCurrentCell.IsEmpty() )
				{
					fcCurrentCell.SetMiss();

				}

			}

		}
	}

	bool CSeaWarModel :: IsShipValid( int iBeginPosition, int iEndPosition, int iShipLenght ) const
	{
		bool bIsValid = true;
		int iRectTopLeft = 0, iRectBottomRight = 0;
		
		do
		{
			if( !IsCellPositionValid( iBeginPosition ) && !IsCellPositionValid( iEndPosition ) )
			{
				bIsValid = false;
				break;
			}
			
			if( ( iEndPosition / k_uiFieldSize != iBeginPosition / k_uiFieldSize ) 
			    &&
			    ( iEndPosition % k_uiFieldSize != iBeginPosition % k_uiFieldSize ) )
			{
				bIsValid = false;
				break;
			}

			if(  ( ( iEndPosition - iBeginPosition + 1 )  != iShipLenght )
				&&
				( ( iEndPosition - iBeginPosition + k_uiFieldSize) / k_uiFieldSize != iShipLenght ) )
			{
				bIsValid = false;
				break;
			}

			if( !IsRectEmpty( GetShipRectTopLeft( iBeginPosition ), GetShipRectBottomRight( iEndPosition ) ) )
			{
				bIsValid = false;
				break;
			}
		}
		while( false );

		return bIsValid;

	}

	bool CSeaWarModel :: SetShip( int iBeginPosition, int iEndPosition, int iCurrentShipLenght )
	{
		
		if( iCurrentShipLenght > 1 )
		{
			int iCellChange = ( iEndPosition - iBeginPosition ) / ( iCurrentShipLenght - 1 );
			
			for( int iCellPosition = iBeginPosition; iCellPosition <= iEndPosition; iCellPosition += iCellChange )
			{
				m_pfOwnField -> CellAtPosition( iCellPosition ).SetShipPart();
			}
		}

		else
		{
			m_pfOwnField -> CellAtPosition( iBeginPosition ).SetShipPart();
		}

		return true;

	}
	
	
	}

	void CSeaWarModel :: SetRandomShips()
	{
		int iShipsPlaced = 0;
		int iBegPos, iEndPos, iMaxPos;
		int iCurrentLenght = m_uiShipsLenghts[ 0 ];

		ClearFields();
		while( iShipsPlaced != k_uiShipsToPlaceCount )
		{
			iMaxPos = k_uiFieldSize - iCurrentLenght + 1;
			iBegPos = ( rand() % iMaxPos ) * k_uiFieldSize +  rand() % iMaxPos;
			( rand() % 2 ) ? iEndPos = iBegPos + iCurrentLenght - 1 : iEndPos = iBegPos + k_uiFieldSize * ( iCurrentLenght - 1 );

			if( IsShipValid( iBegPos, iEndPos, iCurrentLenght ) )
			{
				SetShip( iBegPos, iEndPos, iCurrentLenght );
				++iShipsPlaced;
				iCurrentLenght = m_uiShipsLenghts[ iShipsPlaced ];
			}
		}

	}

	void CSeaWarModel :: ClearFields()
	{
		m_pfOwnField ->ClearCells();
		m_pfEnemyField -> ClearCells();
		m_uiOwnShipsCount = 0;
	}

	int CSeaWarModel :: MakeStep()
	{
		int iStep;

		do
		{
			cout << "Input step: ";
			cin>>iStep;
		}
		while( !IsStepValid( iStep ) );

		return iStep;
	}

	bool CSeaWarModel :: IsStepValid( int iStep )
	{
		bool bValid = IsCellPositionValid( iStep ) && m_pfEnemyField->CellAtPosition( iStep ).IsEmpty();

		return bValid;
	}
	

	bool CSeaWarModel :: IsCellPositionValid( int iCellPosition ) const
	{
		return ( ( iCellPosition >= 0) && ( iCellPosition < m_pfOwnField->CellsCount() ) );
	}
			
	void CSeaWarModel :: InitDefaultShipsLenghts()
	{
		 unsigned int uiShipsLenghts[] = { 4, 3, 3, 2, 2, 2, 1, 1, 1, 1 };
		 memcpy( m_uiShipsLenghts, uiShipsLenghts, k_uiShipsToPlaceCount * sizeof( unsigned int ) );
	}
	
}