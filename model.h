#ifndef MODEL_H
#define MODEL_H

#include "sea_war_precompiled.h"
#include "field.h"


namespace SeaWar
{
	
	class CSeaWarModel
	{
		private:
			static const  int k_uiFieldSize = 10;
			static const  int k_uiShipsToPlaceCount = 10;
			
		private:
			CField *m_pfOwnField;
			CField *m_pfEnemyField;
			unsigned int m_uiShipsLenghts[ k_uiShipsToPlaceCount ];
			unsigned int m_uiOwnShipsCount;

		private:

			const CField* GetOwnField() { return m_pfOwnField; }
			
			CellVector GetPossibleEndPositions( int iBeginPosition, int iShipLenght );

			void PrintCell( const CFieldCell& fcCell ) const;
			void InitDefaultShipsLenghts();
			void MarkDestroyedEnemyShip( int iRectTopLeft, int iRectBottomRight );

			bool IsRectEmpty( int iRectTopLeft, int iRectBottomRight ) const;
			bool IsCellPositionValid( int iCellPosition ) const;
			bool IsShipValid( int iBeginPosition, int iEndPosition, int iShipLenght ) const;
			bool SetShip( int iBeginPosition, int iEndPosition, int iCurrentShipLenght );
			
			bool IsStepValid( int iStep );
			
			int GetShipRectTopLeft(  int iBeginPosition )const;
			int GetShipRectBottomRight( int iEndPosition ) const;

			
		public:
			CSeaWarModel();
			~CSeaWarModel();

		   void ClearFields();
		   void SetShips();
		   void SetRandomShips();

		  
			
	};
	
	
}

#endif /*MODEL_H*/ 